# Echio // io

Echio é uma ferramenta simples escrita em PHP que possibilita testar requisições localmente.

## Instalação

Execute em seu terminal o comando abaixo:
```bash
curl https://gitlab.com/php-developer/echio/raw/master/INSTALL | sh
```

E teremos clonado no diretório `echio` o projeto.

## Inicializar o servidor

Para inicializar o servidor simplesmente execute `make server` na pasta onde o projeto foi clonado.

## Primeiros passos

As requisições realizadas retornam os dados postados em um objeto JSON no seguiunte formato:
```json
{
	url: "url_requisitada",
	get: {
		parametros: "enviados",
		na: "query",
		string: ":)"
	},
	post: {
		parametros: "enviados",
		em: "um",
		form: "submit"
	},
	request: {
		parametros: "enviados",
		geralmente: "por",
		metodos: ["post", "put", "etc"]
	}
}
```

Sendo possível forçar um código de erro/resposta diferente setando o parâmetro query string `force_response` para qualquer valor desejado.
