<?php

header('Content-type: application/json');

if (isset($_GET['force_response']))
	header((isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0')
		." {$_GET['force_response']} Forced."
	);

echo json_encode([
	'url' => $_SERVER['REQUEST_URI'],
	'get' => $_GET,
	'post' => $_POST,
	'request' => $_REQUEST
]);
